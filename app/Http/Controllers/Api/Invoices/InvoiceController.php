<?php

namespace App\Http\Controllers\Api\Invoices;

use App\Domain\Invoices\Actions\CreateInvoiceAction;
use App\Domain\Invoices\DTO\InvoiceData;
use App\Domain\Invoices\DTO\InvoiceRowCollection;
use App\Http\Controllers\Controller;
use App\Http\Requests\Invoices\CreateInvoiceRequest;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function store(
        CreateInvoiceRequest $createInvoiceRequest,
        CreateInvoiceAction $createInvoiceAction
    ){
        $invoiceRowCollection =
            InvoiceRowCollection
                ::fromRequest($createInvoiceRequest);

        $invoiceData =
            InvoiceData
                ::fromRequest($createInvoiceRequest,
                                $invoiceRowCollection);



        $response = $createInvoiceAction->execute($invoiceData);

        if($response instanceof RequestException){
            $data = [
                'message' => json_decode($response
                                        ->getResponse()
                                        ->getBody()
                                        ->getContents())
                                        ->ErrorInformation
                                        ->message
            ];
            return $this->failingResponse($data);
        }

        $data = [
            "data" => $response
        ];
        return $this->successResponse($data);
    }
}
