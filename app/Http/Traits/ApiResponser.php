<?php


namespace App\Http\Traits;


trait ApiResponser
{

    protected function successResponse($data = ['success' => true],
                                       $code = 200){
        $data['success'] = true;
        return response()->json($data, $code);
    }

    protected function failingResponse($data = ['success' => false],
                                       $code = 400){
        $data['success'] = false;
        return response()->json($data, $code);
    }
}
