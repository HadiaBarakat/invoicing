<?php


namespace App\Domain\Invoices\Actions;


use App\Domain\Invoices\DTO\InvoiceData;
use App\Domain\Invoices\Repositories\InvoiceRepository;
use GuzzleHttp\Exception\RequestException;

class CreateInvoiceAction
{
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    public function execute(
        InvoiceData $invoiceData
    ){
        try{
            $invoice = $this->invoiceRepository->create($invoiceData);
        } catch (RequestException $e){
            return $e;
        }
        return $invoice;
    }


}
