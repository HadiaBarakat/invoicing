<?php


namespace App\Domain\Invoices\DTO;


use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class InvoiceRowData extends DataTransferObject
{
    public $ArticleNumber;
    public $DeliveredQuantity;

    public static function fromRequest(
        $invoiceRow
    ) : self {

        return new self([
            'ArticleNumber' => $invoiceRow['article_number'],
            'DeliveredQuantity' => $invoiceRow['delivered_quantity']
        ]);
    }

}
