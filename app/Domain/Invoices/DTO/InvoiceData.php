<?php


namespace App\Domain\Invoices\DTO;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

/*
 * A class to transfer data coming from request to
 * invoice object
 */
class InvoiceData extends DataTransferObject
{

    public $CustomerNumber;
    public $InvoiceRows;

    public static function fromRequest(
        Request $request,
        ?InvoiceRowCollection $invoiceRowCollection
    ) : self {

        $invoiceData = new self([
            'CustomerNumber' => $request['customer_number']
        ]);
        if($invoiceRowCollection){
            $invoiceData->InvoiceRows = $invoiceRowCollection->toArray();
        }
        return $invoiceData;
    }
}
