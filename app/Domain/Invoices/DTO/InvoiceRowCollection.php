<?php


namespace App\Domain\Invoices\DTO;


use Spatie\DataTransferObject\DataTransferObjectCollection;

class InvoiceRowCollection extends DataTransferObjectCollection
{
    public function current(): InvoiceRowData
    {
        return parent::current();
    }

    public static function fromRequest(
        $request
    ): ?self
    {
        $invoiceRows = $request['invoice_rows'];

        if($invoiceRows){
            $invoiceRowsArray = [];

            foreach ($invoiceRows as $invoiceRow){
                array_push($invoiceRowsArray,
                    InvoiceRowData::fromRequest($invoiceRow));
            }
            return new self($invoiceRowsArray);
        }

        return null;
    }

}
