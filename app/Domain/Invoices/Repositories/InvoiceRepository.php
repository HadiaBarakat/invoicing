<?php


namespace App\Domain\Invoices\Repositories;

use App\Domain\Repositories\RepositoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Spatie\DataTransferObject\DataTransferObject;

class InvoiceRepository implements RepositoryInterface
{
    const ACCESS_TOKEN = '95a2e382-de01-4808-9a3b-8a5ddd68dbb7';
    const CLIENT_SECRET = 'CLBkerwoUd';
    const BASE_URL = 'https://api.fortnox.se/3/invoices';

    const headers = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'Access-Token' => self::ACCESS_TOKEN,
        'Client-Secret' => self::CLIENT_SECRET,
    ];
    private $client;


    public function __construct()
    {
        $this->client = new Client([
            'headers' => self::headers
        ]);
    }


    public function create(DataTransferObject $dataTransferObject)
    {
       $body = [
           'Invoice' => $dataTransferObject->toArray()
       ];

        try{
            $response = $this->client->request('POST',
                      self::BASE_URL, [
                        'json' => $body
            ]);
        }  catch (RequestException $e) {
            throw $e;
        }

        return json_decode($response->getBody()->getContents());
    }

}
