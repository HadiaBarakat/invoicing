<?php


namespace App\Domain\Repositories;


use Spatie\DataTransferObject\DataTransferObject;

interface RepositoryInterface
{
    public function create(DataTransferObject $dataTransferObject);
}
